function validaCpf(cpf) {
  var Soma;
  var Resto;
  Soma = 0;
  if (cpf.length !== 11 ||
    cpf == "00000000000" ||
    cpf == "11111111111" ||
    cpf == "22222222222" ||
    cpf == "33333333333" ||
    cpf == "44444444444" ||
    cpf == "55555555555" ||
    cpf == "66666666666" ||
    cpf == "77777777777" ||
    cpf == "88888888888" ||
    cpf == "99999999999")
    return false;

  for (i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

  if ((Resto == 10) || (Resto == 11)) Resto = 0;
  if (Resto != parseInt(cpf.substring(9, 10))) return false;

  Soma = 0;
  for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;

  if ((Resto == 10) || (Resto == 11)) Resto = 0;
  if (Resto != parseInt(cpf.substring(10, 11))) return false;
  return true;
}

function validaCnpj(cnpj) {
  cnpj = cnpj.replace(/[^\d]+/g, '');

  if (cnpj == '') return false;

  if (cnpj.length != 14)
    return false;

  if (cnpj == "00000000000000" ||
    cnpj == "11111111111111" ||
    cnpj == "22222222222222" ||
    cnpj == "33333333333333" ||
    cnpj == "44444444444444" ||
    cnpj == "55555555555555" ||
    cnpj == "66666666666666" ||
    cnpj == "77777777777777" ||
    cnpj == "88888888888888" ||
    cnpj == "99999999999999")
    return false;

  cnpjLengh = cnpj.length - 2
  number = cnpj.substring(0, cnpjLengh);
  d = cnpj.substring(cnpjLengh);
  soma = 0;
  pos = cnpjLengh - 7;
  for (i = cnpjLengh; i >= 1; i--) {
    soma += number.charAt(cnpjLengh - i) * pos--;
    if (pos < 2)
      pos = 9;
  }
  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
  if (resultado != d.charAt(0))
    return false;

  cnpjLengh = cnpjLengh + 1;
  number = cnpj.substring(0, cnpjLengh);
  soma = 0;
  pos = cnpjLengh - 7;
  for (i = cnpjLengh; i >= 1; i--) {
    soma += number.charAt(cnpjLengh - i) * pos--;
    if (pos < 2)
      pos = 9;
  }
  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
  if (resultado != d.charAt(1))
    return false;

  return true;
}

function validaDocumento(documento) {
  window.document.form.numeral.value = "";
  const tipoDocumento = window.document.form.tipo_documento.value;
  if (tipoDocumento == 1) {
    if (validaCpf(documento)) {
      window.document.form.numeral.value = "Documento Válido";
    } else {
      window.document.form.numeral.value = "Documento Inválido";
    }
  } else {
    if (validaCnpj(documento)) {
      window.document.form.numeral.value = "Documento Válido";
    } else {
      window.document.form.numeral.value = "Documento Inválido";
    }
  }
}