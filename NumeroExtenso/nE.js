function removeAcento(c) {
  var ex = [
    ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
    ["dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"],
    ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
    ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
  ];
  var pri, seg, terc, quart, seg = c.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
  for (var f = seg.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []) {
    j && (seg[j] = (("." + seg[j]) * 1).toFixed(2).slice(2));
    if (!(pri = (terc = seg[j]).slice((l = terc.length) % 3).match(/\d{3}/g), terc = l % 3 ? [terc.slice(0, l % 3)] : [], terc = pri ? terc.concat(pri) : terc).length) continue;
    for (pri = -1, l = terc.length; ++pri < l; t = "") {
      if (!(quart = terc[pri] * 1)) continue;
      quart % 100 < 20 && (t += ex[0][quart % 100]) ||
        quart % 100 + 1 && (t += ex[1][(quart % 100 / 10 >> 0) - 1] + (quart % 10 ? e + ex[0][quart % 10] : ""));
      s.push((quart < 100 ? t : !(quart % 100) ? ex[2][quart == 100 ? 0 : quart / 100 >> 0] : (ex[2][quart / 100 >> 0] + e + t)) +
        ((t = l - pri - 2) > -1 ? " " + (quart > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
    }
    pri = ((sl = s.length) > 1 ? (pri = s.pop(), s.join(" ") + e + pri) : s.join("") || ((!j && (seg[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
    pri && r.push(pri + (c ? (" " + (terc.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(seg[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
  }
  window.document.form.texto_formatado.value = r.join(e);
}