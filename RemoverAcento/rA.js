function removeAcento(texto) {
  var texto_formatado = texto.normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/[^a-zA-Zs]/g, " ");
  window.document.form.texto_formatado.value = texto_formatado;
}