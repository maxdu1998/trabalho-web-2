function calcular(op, his) {
    var valor1 = document.cB.valor1.value;
    var valor2 = document.cB.valor2.value;
    var res;

    if (op == "somar") {
        res = parseInt(valor1) + parseInt(valor2);
    } else {
        if (op == "subtrair") {
            res = valor1 - valor2;
        } else {
            if (op == "multiplicar") {
                res = valor1 * valor2;
            } else {
                if (valor2 == 0 || valor2 == "") {
                    res = "Impossível dividir por 0 ou vazio";
                } else {
                    res = valor1 / valor2;
                }
            }
        }
    }

    document.cB.res.value = res;
    // Verifica se já possui registro vazio no histórico, se sim substitui, se não concatena
    if (his == "") {
        document.cB.his.value = String(res);
    } else {
        if (res != "Impossível dividir por 0 ou vazio") {
            document.cB.his.value = String(his) + "\n" + String(res);
        }
    }
}