function contaTexto(texto) {
  //removendo quebra de linha e espaco em branco para contar o numero de caracteres
  var numeroCaracter = texto.replace(/ /g, '').replace(/(\r\n|\n|\r)/gm, '').length;
  window.document.form.caracteres.value = numeroCaracter;
  var arrayPalavras = texto.split(' ');
  var numeroPalavras = 0;
  arrayPalavras.forEach(element => {
    if (element) {
      numeroPalavras++;
    }
  });
  window.document.form.palavras.value = numeroPalavras;

  var numeroEspacos = 0;
  for (const element in texto) {
    if (texto.hasOwnProperty(element)) {
      if (texto[element] == " ") {
        numeroEspacos++;
      }

    }
  }
  window.document.form.espacos.value = numeroEspacos;

  var numeroLinhas = texto.split("\n").length;
  window.document.form.linhas.value = numeroLinhas;

  var numeroVogais = texto.match(/[aeiou]/gi);
  window.document.form.vogais.value = (numeroVogais ? numeroVogais.length : 0);

  var quantidadeNumero = texto.replace(/[^0-9]/g, '').length;
  window.document.form.numeros.value = quantidadeNumero;


}